<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'first_name',
        'middle_name',
        'last_name',
        'first_address',
        'last_address',
        'city',
        'state',
        'zip',
        'phone_number',
        'phone_type',
        'sex',
        'dob',
        'ssn'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User');
    }

}
