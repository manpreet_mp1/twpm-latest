<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{

    protected $fillable = ['heading_subfooter', 'phone', 'footer_text', 'navbar2_text', 'navbar2_link'];
    protected $table = 'footer';
    public $timestamps = true;

}
