<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Privacy extends Model
{

    protected $fillable = ['content'];
    protected $table = 'privacy';
    public $timestamps = true;

}