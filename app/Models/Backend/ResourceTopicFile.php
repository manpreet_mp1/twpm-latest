<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class ResourceTopicFile extends Model
{

    protected $fillable = ['resource_topic_id', 'file_text', 'upload_file'];
    protected $table = 'resource_topics_files';
    public $timestamps = true;
    
    public function resourceTopics(){
        return $this->belongsTo('App\Models\Backend\ResourceTopic');
    }

}
