<?php
namespace App\Models\Backend;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model {
    protected $fillable = [
        'name',
        'order',
        'title',
        'description',
        'twitter',
        'phone',
        'linkedin',
        'email',
        'image'
    ];
    protected $table = 'employees';
    public $timestamps = true;
}
