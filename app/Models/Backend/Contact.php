<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    protected $fillable = ['address', 'phone'];
    protected $table = 'contact';
    public $timestamps = true;

}