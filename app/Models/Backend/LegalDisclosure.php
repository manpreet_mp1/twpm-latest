<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class LegalDisclosure extends Model
{

    protected $fillable = ['content'];
    protected $table = 'legal_disclosures';
    public $timestamps = true;

}