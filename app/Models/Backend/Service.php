<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Backend\ServiceFile;
use App\Models\Backend\ServiceServiceCategory;

class Service extends Model
{

    protected $fillable = ['title','title_bg_color', 'order', 'image', 'description', 'fee_schedule' , 'recommended_for' , 'cost'];
    protected $table = 'services';
    public $timestamps = true;

    use SoftDeletes;
    
    public function serviceCategories(){
        return $this->belongsToMany('App\Models\Backend\ServiceCategory','service_service_categories');
    }
    
    public function serviceFiles(){
        return $this->hasMany('App\Models\Backend\ServiceFile');
    }
    
}
