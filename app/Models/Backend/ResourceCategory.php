<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResourceCategory extends Model
{

    protected $fillable = ['title','title_bg_color','order', 'image'];
    protected $table = 'resource_categories';
    public $timestamps = true;

    use SoftDeletes;
    
    public function resourceTopics(){
        return $this->hasMany('App\Models\Backend\ResourceTopic');
    }
    
}
