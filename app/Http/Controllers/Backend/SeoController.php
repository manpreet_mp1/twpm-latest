<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\SaveSeoRequest;
use App\Models\Backend\Seo;
use Redirect;

class SeoController extends Controller
{

    public function index()
    {
        $checkEmpty = Seo::get()->count();

        if (!$checkEmpty) {
            $seoContent = $checkEmpty;
        } else {
            $seoContent = Seo::first()->toArray();
        }
        return view('backend.seo.index', compact('seoContent'));
    }
    
    public function saveSeo(SaveSeoRequest $request)
    {
        $input = $request->input();
        
        if (Seo::get()->count()) {
            $currentContent = Seo::first();
            $fileArray = (!empty($request->file())) ? $this->_uploadImages($request->file()) : [];
            $input['home_seo_img'] = (!empty($fileArray['home_seo_img'])) ? $fileArray['home_seo_img'] : $currentContent['home_seo_img'];
            $input['about_seo_img'] = (!empty($fileArray['about_seo_img'])) ? $fileArray['about_seo_img'] : $currentContent['about_seo_img'];
            $currentContent->update($input);
            return Redirect::back()->withFlashSuccess('The SEO content has been updated.');
        } else {
            $fileArray = (!empty($request->file())) ? $this->_uploadImages($request->file()) : [];
            $input['home_seo_img'] = $fileArray['home_seo_img'];
            $input['about_seo_img'] = $fileArray['about_seo_img'];
            $saveSeo = Seo::create($input);
            return Redirect::back()->withFlashSuccess('The SEO content has been saved.');
        }
    }

    private static function _uploadImages($files)
    {
        $fileArray = [];
        foreach ($files as $key => $file) {
            $fileName = date('d-m-y-h-i-s-') . $file->getClientOriginalName();
            $file->move('img/backend/seo', $fileName);
            $fileArray[$key] = $fileName;
        }
        return $fileArray;
    }

}