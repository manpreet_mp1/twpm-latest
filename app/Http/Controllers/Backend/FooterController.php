<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\SaveFooterRequest;
use App\Models\Backend\Footer;
use Redirect;

class FooterController extends Controller
{

    public function index()
    {
        $checkEmpty = Footer::get()->count();
        if (!$checkEmpty) {
            $footerContent = $checkEmpty;
        } else {
            $footerContent = Footer::first()->toArray();
        }
        return view('backend.footer.index', compact('footerContent'));
    }

    public function saveFooter(SaveFooterRequest $request)
    {
        $input = $request->input();
        if (Footer::get()->count()) {
            $footer = Footer::first();
            $footer->update([
                'heading_subfooter' => $input['heading_subfooter'],
                'phone' => $input['phone'],
                'footer_text' => $input['footer_text'],
                'navbar2_text' => $input['navbar2_text'],
                'navbar2_link' => $input['navbar2_link'],
            ]);
            return Redirect::back()->withFlashSuccess('The footer/Navbar content has been updated.');
        } else {
            Footer::create([
                'heading_subfooter' => $input['heading_subfooter'],
                'phone' => $input['phone'],
                'footer_text' => $input['footer_text'],
                'navbar2_text' => $input['navbar2_text'],
                'navbar2_link' => $input['navbar2_link'],
            ]);
            return Redirect::back()->withFlashSuccess('The footer/Navbar content has been saved.');
        }
    }

}
