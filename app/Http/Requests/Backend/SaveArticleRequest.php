<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class SaveArticleRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'excerpt' => 'required',
            'body' => 'required',
            'slug' => 'required|max:255',
            'blog_category_id' => 'required|array|min:1',
            'author' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title is required.',
            'excerpt.required' => 'Excerpt is required.',
            'body.required' => 'Body is required.',
            'slug.required' => 'Slug is required.',
            'blog_category_id.required' => 'Atleast 1 Blog Category is required.',
            'author.required' => 'Author is required.'
        ]; 
    }

}
