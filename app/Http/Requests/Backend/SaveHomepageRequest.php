<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class SaveHomepageRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'heading_1' => 'required',
//            'subheading_1' => 'required|max:255',
            'heading_3' => 'required',
//            'subheading_3' => 'required|max:255',
            'heading_4' => 'required',
            'description_4' => 'required|max:255',
            'linktext_4' => 'required|max:255',
            'heading_6' => 'required|max:255',
            'description_6' => 'required|max:255',
            'linktext_6' => 'required|max:255',
            'heading_7' => 'required|max:255',
            'box_heading' => 'required',
            'box_description' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'heading_1.required' => 'Heading is required',
//            'subheading_1.required' => 'Sub-Heading is required',
            'heading_3.required' => 'Heading is required',
//            'subheading_3.required' => 'Sub-Heading is required',
            'heading_4.required' => 'Heading is required',
            'description_4.required' => 'Description is required',
            'linktext_4.required' => 'Link Text is required',
            'heading_6.required' => 'Heading is required',
            'description_6.required' => 'Description is required',
            'linktext_6.required' => 'Link Text is required',
            'heading_7.required' => 'Heading is required',
            'box_heading.required' => 'Heading is required',
            'box_description.required' => 'Description is required'
        ]; 
    }

}
