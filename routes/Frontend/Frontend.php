<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'
 */
//Route::get('/', 'FrontendController@index')->name('index');
//Route::get('macros', 'FrontendController@macros')->name('macros');

Route::get('/', 'HomeController@index')->name('index');
Route::get('/services', 'HomeController@services')->name('services');
Route::get('/services/{id}', 'HomeController@getServiceDetails')->name('getServiceDetails');
Route::post('/contact', 'HomeController@postContact')->name('postContact');
Route::get('/resources/{id}', 'HomeController@getResourceTopics')->name('getResourceTopics');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/resources', 'HomeController@resources')->name('resources');
Route::get('/blogs', 'HomeController@blog')->name('blog');
Route::get('/blog/{slug?}', 'HomeController@singleBlog')->name('single-blog');
Route::get('/moreBlogs/{skip?}', 'HomeController@getMoreBlogs')->name('getMoreBlogs');
Route::get('/privacy-and-security', 'HomeController@privacy')->name('privacy');
Route::get('/terms-of-use', 'HomeController@terms')->name('terms');
Route::get('/legal-disclosures', 'HomeController@legal')->name('legal');
Route::post('/subscribe', 'HomeController@subscribe')->name('subscribe');
Route::post('/share', 'HomeController@share')->name('share');
Route::post('/investmentIntake', 'HomeController@investmentIntake')->name('investmentIntake');


Route::group(['prefix' => 'client'], function () {

});

Route::post('stepOneRegister', 'Auth\RegisterController@register')->name('client.stepOneRegister');
Route::post('stepTwoRegister', 'ClientController@storeSignupStepTwo')->name('client.stepTwoRegister');
Route::post('sendOtp', 'ClientController@sendOtp')->name('client.sendOtp');
Route::post('verifyOtp', 'ClientController@verifyOtp')->name('client.verifyOtp');
Route::post('stepFourRegister', 'ClientController@storeSignupStepFour')->name('client.stepFourRegister');
Route::post('stripePayment', 'ClientController@stripePayment')->name('stripePayment');

//subdomain routes
Route::group(['domain' => '{domain}.twpm.rahulbehl.me'], function () {

    Route::get('signupStepOne', 'ClientController@signupStepOne')->name('client.signupStepOne');
    Route::get('signupStepTwo', 'ClientController@signupStepTwo')->name('client.signupStepTwo');
    Route::get('signupStepThree', 'ClientController@signupStepThree')->name('client.signupStepThree');
    Route::get('signupStepFour', 'ClientController@signupStepFour')->name('client.signupStepFour');
    Route::get('signupStepFive', 'ClientController@signupStepFive')->name('client.signupStepFive');
    Route::get('signupStepSix', 'ClientController@signupStepSix')->name('client.signupStepSix');

    Route::group(['middleware' => 'guest'], function () {
        Route::get('login', function () {
            return view('frontend.client.login');
        })->name('client.login');
    });


    Route::group(['middleware' => 'auth'], function () {
        Route::get('dashboard', function () {
            return view('frontend.client.dashboard');
        })->name('client.dashboard');
         
        Route::get('clientResources', function () {
            return view('frontend.client.resources');
        })->name('client.resources');
        
        Route::get('clientProfile', function () {
            return view('frontend.client.profile');
        })->name('client.profile');
        
    });
});


Route::group(['middleware' => 'guest'], function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
});



/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function() {
        /**
         * User Dashboard Specific
         */
//        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /**
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /**
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');
    });

//    Route::get('client/dashboard', 'ClientController@clientDashboard')->name('client.dashboard');
});
