$(document).ready(function () {
    //show single service
    $('.service-box').on('click', function () {
        var $this = $(this);
        $this.parent().addClass('service-open-slide');
        $('body').animate({scrollTop: 0}, 500);
        $.ajax({
            url: $this.attr('data-url'),
            success: function (data) {
                var appendlocation = $('#single-service'),
                        single_service_template = $('#single-service-html').html();
                single_service_template = _.template(single_service_template);
                var topics = {
                    single_service: data
                };
                console.log(topics);
                appendlocation.empty().append(single_service_template(topics));
                $('#single-service').animate({'marginLeft': '-35%', opacity: 1}, 700, 'easeInOutCubic');
                $('#all-services').animate({'marginLeft': '-15%', opacity: 0.3}, {
                    duration: 700,
                    complete: function () {
                        $('#all-services').css('height', '1px');
                    },
                    easing: 'easeInOutCubic'
                });
                $this.parent().removeClass('service-open-slide');
                return false;
            },
            error: function () {
                swal({
                    title: 'Oops!!',
                    text: 'Something went wrong. Please try again.',
                    type: 'error',
                    customClass: ''
                });
            }
        });
    });
    $('#single-service').on('click', '.back-services', function () {
        $('#all-services,#single-service').css('height', 'auto').animate({'marginLeft': '0%', opacity: 1}, {
            duration: 700,
            complete: function () {
                $('#single-service').html('');
            },
            easing: 'easeInOutCubic'
        });
    });
    //fee-schedule modal
    $('#single-service').on('click', '.fee-schedule', function () {
        $('#fee-schedule-modal').modal();
    });
    $('#single-service').on('click', '.get-started', function () {
        $('#get-started-modal').modal();
    });
    if (route) {
        $('.service-box[data-url="' + route + '"]').trigger('click');
    }

    $('.service-img h3').each(function () {
        var top = (($(this).closest('.service-img').height()) / 2) - (($(this).height()) / 2);
        console.log(top);
        $(this).css('top', top);
    });
});