/**
 * Place the CSRF token as a header on all pages for access in AJAX requests
 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//END
//Show contact modal
$(document).on('click', '.contact-modal-show', function (e) {
    $('#contact-us').show();
    $('.contact-response').html('');
    $('#get-started-modal').modal();
    e.preventDefault();
});
//END 
//Contact form submit
$('#contact-us').on('submit', function (e) {
    e.preventDefault();
    var $this = $(this);
    $.ajax({
        url: $this.attr('action'),
        type: 'POST',
        data: $this.serialize(),
        success: function (resp) {
            $this.parent().find('.contact-response').html("<p>Thank you for contacting us. We'll respond to you shortly.</p>");
            $this.hide();
        },
        error: function (err) {
            swal({
                title: 'Oops!!',
                text: 'Something went wrong. Please try again.',
                type: 'error',
                customClass: ''
            });
        }
    });
});
//END
//Contact form submit
$('#investment-intake-form').on('submit', function (e) {
    e.preventDefault();
    //Error empty
    $('.contact-response').removeClass('text-danger').text('');

    var $this = $(this),
            consent = $('span.select-consent').text(),
            level = $('span.select-level').text(),
            query_type = $('span.select-query_type').text(),
            name = $('input[name="name"]').val(),
            phone = $('input[name="phone"]').val(),
            email = $('input[name="email"]').val(),
            gRecaptcha = $('[name="g-recaptcha-response"]').val();
    $.ajax({
        url: $this.attr('action'),
        type: 'POST',
        data: {
            consent: consent,
            level: level,
            query_type: query_type,
            name: name,
            phone: phone,
            email: email,
            gRecaptcha: gRecaptcha
        },
        success: function (resp) {
            $this.parent().find('.response').html("<p>Thank you for contacting us. We'll get back to you shortly.</p>");
            $this.hide();
        },
        error: function (err) {
            var error = JSON.parse(err.responseText);
            if (typeof error.gRecaptcha != 'undefined') {
                $('.contact-response').addClass('text-danger').text('reCAPTCHA is Required.');
            } else {
                swal({
                    title: 'Oops!!',
                    text: 'Something went wrong. Please try again.',
                    type: 'error',
                    customClass: ''
                });
            }
        }
    });
});
//END

$(document).ready(function () {
    $('.select-input').each(function () {
        $(this).text($(this).next().children().first().text());
    });
    $('.select-input').on('click', function () {
        $(this).next().addClass('ul-border').show();
        $(this).next().children().show();
    });

    $(document).on('click', function (e) {
        var lists = $('.select-list');
        if (!lists.is(e.target) && !$('.select-input').is(e.target))
        {
            lists.hide();
        }
    });

    $('.select-list').each(function () {
        var li_lengths = [],
                list = $(this);
        list.children('li').each(function () {
            li_lengths.push($(this).outerWidth());
        });
        var max_width = Math.max.apply(Math, li_lengths);
        list.parent('.investment_wrapper').width(max_width);
        list.parent('.investment_wrapper').find('.select-input').width(max_width);
    });

    $('#investment-intake-form').on('click', 'li', function () {
        $(this).parent().removeClass('ul-border');
        var selected_val = $(this).data('val'),
                input_class = $(this).parent().data('input');
        $('.' + input_class).text(selected_val);
        $(this).parent().children().hide();
    });
    $('.proceed').on('click', function () {
        $('.dialogue-1').hide();
        $('.dialogue-2').show();
    });
    $('.back').on('click', function () {
        $('.dialogue-2').hide();
        $('.dialogue-1').show();
    });
    $('#investment-intake-form').on('keyup', '.contact-input', function () {
        if ($('#name-input').val() !== '' && $('#phone-input').val() !== '' && $('#email-input').val() !== '') {
            $('.submit_form').prop('disabled', false);
        } else {
            $('.submit_form').prop('disabled', true);
        }
    });
});