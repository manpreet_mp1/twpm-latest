@extends('backend.layouts.master')

@section ('title', 'SEO' )

@section('page-header')
<h1>
    {{ app_name() }}
    <small>SEO</small>
</h1>
@endsection

@section('after-styles')
{{ Html::style("css/backend/plugin/html5imageupload/html5imageupload.css") }}
@stop

@section('content')
{{ Form::model($seoContent,['id'=>'seo_form','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
<!-- Home Page SEO -->
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Home - SEO</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('home_seo_title')) echo ' has-error'; ?>">
            {{ Form::label('home_seo_title','Title', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('home_seo_title', null, ['class' => 'form-control', 'placeholder' => 'Title']) }}
                <span class="help-block">{{ $errors->first('home_seo_title') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('home_seo_description')) echo ' has-error'; ?>">
            {{ Form::label('home_seo_description','Description', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('home_seo_description', null, ['class' => 'form-control', 'placeholder' => 'Description']) }}
                <span class="help-block">{{ $errors->first('home_seo_description') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('home_seo_img')) echo ' has-error'; ?>">
            {{ Form::label('home_seo_img','Image', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-12">
                        @if($seoContent)
                        <input type="file" name="home_seo_img" />
                        @else
                        <input type="file" name="home_seo_img" required/>
                        @endif
                        <span class="help-block">{{ $errors->first('home_seo_img') }}</span>
                    </div>
                    @if($seoContent)
                    <div class="col-lg-12">
                        <img src="{{URL::asset('img/backend/seo/'.$seoContent['home_seo_img'])}}" style="max-width:50%; max-height:200px;"/>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<!-- About -->
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Who We Are - SEO</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('about_seo_title')) echo ' has-error'; ?>">
            {{ Form::label('about_seo_title','Title', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('about_seo_title', null, ['class' => 'form-control', 'placeholder' => 'Title']) }}
                <span class="help-block">{{ $errors->first('about_seo_title') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('about_seo_description')) echo ' has-error'; ?>">
            {{ Form::label('about_seo_description','Description', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('about_seo_description', null, ['class' => 'form-control', 'placeholder' => 'Description']) }}
                <span class="help-block">{{ $errors->first('about_seo_description') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('about_seo_img')) echo ' has-error'; ?>">
            {{ Form::label('about_seo_img','Image', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-12">
                        @if($seoContent)
                        <input type="file" name="about_seo_img" />
                        @else
                        <input type="file" name="about_seo_img" required/>
                        @endif
                        <span class="help-block">{{ $errors->first('about_seo_img') }}</span>
                    </div>
                    @if($seoContent)
                    <div class="col-lg-12">
                        <img src="{{URL::asset('img/backend/seo/'.$seoContent['about_seo_img'])}}" style="max-width:50%; max-height:200px;"/>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.seo', 'Undo Changes', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit('Save Changes', ['class' => 'btn btn-success submit_form']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{{ Form::close() }}
@endsection