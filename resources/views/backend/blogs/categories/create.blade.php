@extends('backend.layouts.master')
@if(isset($category))
@section ('title', 'Edit Blog Category' )
@else
@section ('title', 'Create Blog Category' )
@endif

@section('page-header')
<h1>
    {{ app_name() }}
    @if(isset($category))
    <small>Edit Blog Category</small>
    @else
    <small>Create Blog Category</small>
    @endif
</h1>
@endsection

@section('content')
@if(isset($category))
{{ Form::model($category,['id'=>'blog_categories_form','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
{{ Form::hidden('id',$category->id) }}
@else
{{ Form::open(['id'=>'blog_categories_form','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
@endif
<div class="box box-success"><!-- Slide-1 -->
    <div class="box-header with-border">
        @if(isset($category))
        <h3 class="box-title">Edit Blog Category</h3>
        @else
        <h3 class="box-title">Create Blog Category</h3>
        @endif
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('title')) echo ' has-error'; ?>">
            {{ Form::label('title','Title', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title', 'maxlength'=>'120']) }}
                <span class="help-block">{{ $errors->first('title') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('description')) echo ' has-error'; ?>">
            {{ Form::label('description','Description', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('description', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Description']) }}
                <span class="help-block">{{ $errors->first('description') }}</span>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.blogs.categories', 'Cancel', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            @if(isset($category))
            {{ Form::submit('Update Blog Category', ['class' => 'btn btn-success submit_form']) }}
            @else
            {{ Form::submit('Save Blog Category', ['class' => 'btn btn-success submit_form']) }}
            @endif
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{{ Form::close() }} 
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/ckeditor/ckeditor.js") }}
<script>
</script>
@endsection