@extends('backend.layouts.master')  

@section ('title', 'Footer') 

@section('page-header')
<h1>
    {{ app_name() }}
    <small>Footer</small>
</h1>
@endsection

@section('after-styles')
@stop

@section('content')

{{ Form::model($footerContent,['id'=>'footer_form','class' => 'form-horizontal','role' => 'form']) }}

<div class="box box-success"><!-- Subfooter -->
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('strings.backend.home.subfooter') }}</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('heading_subfooter')) echo ' has-error'; ?>">
            {{ Form::label('heading_subfooter',trans('strings.backend.home.subfooter_heading'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('heading_subfooter', null, ['class' => 'form-control ckeditor', 'placeholder' => trans('strings.backend.home.subfooter_heading')]) }}
                <span class="help-block">{{ $errors->first('heading_subfooter') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('phone')) echo ' has-error'; ?>">
            {{ Form::label('phone',trans('strings.backend.home.phone_number'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('phone', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.phone_number')]) }}
                <span class="help-block">{{ $errors->first('phone') }}</span>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-success"><!-- Footer -->
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('strings.backend.home.footer') }}</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('footer_text')) echo ' has-error'; ?>">
            {{ Form::label('footer_text',trans('strings.backend.home.footer_text'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('footer_text', null, ['class' => 'form-control ckeditor', 'placeholder' => trans('strings.backend.home.footer_text')]) }}
                <span class="help-block">{{ $errors->first('footer_text') }}</span>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-success"><!-- Navbar2 -->
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('strings.backend.home.navbar') }}</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('footer_text')) echo ' has-error'; ?>">
            {{ Form::label('navbar2_text',trans('strings.backend.home.navbar_text'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('navbar2_text', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.navbar_text')]) }}
                <span class="help-block">{{ $errors->first('navbar_text') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('footer_text')) echo ' has-error'; ?>">
            {{ Form::label('navbar2_link',trans('strings.backend.home.navbar_link'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('navbar2_link', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.navbar_link')]) }}
                <span class="help-block">{{ $errors->first('navbar_link') }}</span>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.footer', 'Undo Changes', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit('Save Changes', ['class' => 'btn btn-success submit_form']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{{ Form::close() }}
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/ckeditor/ckeditor.js") }}
<script>
</script>
@endsection