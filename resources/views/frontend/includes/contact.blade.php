<div id="get-started-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">  
            <span class="close" data-dismiss="modal">&times;</span>
            <div class="modal-body"> 
                <h3>Contact Us</h3> 
                <div class="col-sm-6 left-side"> 
                    <div class="col-sm-12 call">
                        <div class="icon"><span><i class="fa fa-phone" aria-hidden="true"></i></span>call us</div>
                        <p><span><?php echo (!empty($contact) ? $contact->phone : '') ?></span></p>
                    </div>
                    <div class="col-sm-12 chat">
                        <div class="icon"><span><i class="fa fa-inbox" aria-hidden="true"></i></span>live chat</div>
                        <a class="btn-green trigger-chat">LET'S TALK</a> 
                    </div>
                    <div class="col-sm-12 offices">
                        <div class="icon"><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>our offices</div>
                        <?php echo (!empty($contact) ? $contact->address : '') ?>
                    </div>
                </div>
                <div class="col-sm-6 right-side">
                    <div class="contact-response"></div>
                    {{Form::open(['route' => 'frontend.postContact','id'=>'contact-us','class' => 'contact-form', 'role' => 'form', 'method' => 'POST'])}}
                    <div class="form-group">
                        {{ Form::select('query_type', ['I have general questions about investing or financial matters'=>'I have general questions about investing or financial matters','I need help with managing my money and debts'=> 'I need help with managing my money and debts','I need help planning for college'=> 'I need help planning for college','I need help planning for retirement'=>'I need help planning for retirement','I need help with my 401(k) choices'=>'I need help with my 401(k) choices','I would like to discuss my portfolio'=>'I would like to discuss my portfolio','Other'=>'Other'], null, ['class' => 'form-control select-type', 'placeholder' => 'How can we help?', 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Your name', 'required']) }}
                    </div>
                    <div class="form-group"> 
                        {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Your email', 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Your phone', 'required']) }}
                    </div>
                    {{ Form::submit('Submit', ['class' => 'form-control btn-green']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div> 