 @include('frontend.includes.contact')
<div class="col-sm-12" id="footer"> 
    <div class="container">
        <ul class="footer-links">
            <li class="hide"><a href="https://www.facebook.com/">Like <span class="facebook-icon"></span></a></li>
            <li class="hide"><a href="https://twitter.com/">Follow <span class="twitter-icon"></span></a></li>
            <li><a href="{{ route('frontend.legal') }}">Legal Disclosures</a></li>
            <li><a href="{{ route('frontend.privacy') }}">Privacy & Security</a></li>
            <li><a href="{{ route('frontend.terms') }}">Terms of Use</a></li>
            <li><a href="" class="contact-modal-show">Contact</a></li>
        </ul>
        <div class="footer-text">
            <?php echo (!empty($footer) ? $footer->footer_text : '')?>
        </div>
    </div>
</div> 