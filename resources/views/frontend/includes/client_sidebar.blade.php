<div class="client-sidebar">
    <div class="site-logo">
        {{ HTML::image('img/icon-hires.png') }}
    </div>
    <ul class="list-unstyled">
        <li class="active">
            <a href="">
                <div class="icon"> <i class="fa fa-home"></i> </div>
                Home
            </a>
        </li>
        <li>
            <a href="">
                <div class="icon"><i class="fa fa-briefcase"></i> </div>
                My Services
            </a>
        </li>
        <li>
            <a href="">
                <div class="icon"><i class="fa fa-upload"></i><span class="noti-red"></span> </div>
                Documents
            </a>
        </li>
        <li>
            <a href="">
                <div class="icon"><i class="fa fa-folder-open"></i></div>
                Resources
            </a>
        </li>
    </ul>

</div>