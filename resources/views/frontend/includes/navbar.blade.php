<nav class="navbar home-navbar">
    <div class="container">
        <div class="navbar-header"> 
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#frontend-navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>    
            </button>
            <a href='{{route('frontend.index')}}' class="navbar-brand">{{HTML::image('/img/Logo-wht.png','alt',['class'=>"logo-img"])}}</a>
        </div>
        <div class="collapse navbar-collapse" id="frontend-navbar-collapse">
            <ul class="nav navbar-nav">
                <li class='{{Route::getCurrentRoute()->getName()=='frontend.about'?'active':''}}'>{{ link_to_route('frontend.about', trans('navbar.home.who_we')) }}</li>
                <li class='{{Route::getCurrentRoute()->getName()=='frontend.services'?'active':''}}'>{{ link_to_route('frontend.services', trans('navbar.home.services')) }}</li>
                <li class='{{Route::getCurrentRoute()->getName()=='frontend.resources'?'active':''}}'>{{ link_to_route('frontend.resources', trans('navbar.home.resources')) }}</li>
                <li class='{{Route::getCurrentRoute()->getName()=='frontend.blog'?'active':''}}'>{{ link_to_route('frontend.blog', trans('navbar.home.commentary')) }}</li>
            </ul>
            <ul class="nav navbar-right">
                <li><a href="" class="contact-modal-show">Contact Us</a></li>
                    <?php if (!empty($footer)) { ?>
                    <li> <a href="{{$footer->navbar2_link}}">{{ $footer->navbar2_text }}</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>