<div class="client-header col-sm-12">
    <div class="heading pull-left">My Total Wealth</div>
    <ul class="actions-list list-unstyled pull-right list-inline">
        <li>
            <input type="text" id="header-search" />
            <i class="fa fa-search"></i>
        </li>
        <li>
            <i class="fa fa-bell-o"></i>
            <span class="noti-red"></span>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Username <i class="fa fa-angle-down"></i> </a>
            <ul class="dropdown-menu">
                <li><a href="#"> <i class="fa fa-user"></i> My Profile</a></li>
                <li><a href="#"> <i class="fa fa-cog"></i> Settings</a></li>
                <li><a href="#"> <i class="fa fa-question-circle"></i> Help</a></li>
                <li><a href="#"> <i class="fa fa-sign-out"></i> Logout</a></li>
            </ul>
        </li>
    </ul>
</div>