@extends('frontend.layouts.master')

@section('title')
Commentary &#8226; Total Wealth Planning Management
@stop

@section('after-styles')
{{ Html::style(elixir('css/blog.css')) }}
@stop 

@section('content')
<?php

use Carbon\Carbon;
?>
<div class="col-sm-12" id="blog">
    <div class="blue-bg  nav-change">
        <h3>Where Investors go to <span>grow</span></h3>
        <a class="btn-white-bordered" id="subscribe-newsletter">Subscribe</a>
    </div>
    <div class="col-sm-12 blog-news">
        <div class="container">
            <h1 class="heading">Latest News</h1>
            <?php foreach ($firstThreeArticles as $article) { ?>
                <div class="col-sm-4">
                    <a href="{{ route('frontend.single-blog',$article->slug) }}">
                        <div class="commentary-box" style="background-image: url('img/backend/blogs/articles/{{ $article->image }}')">
                            <div class="black-bg">
                                <p>
                                    {{Carbon::createFromTimeStamp(strtotime($article->publish_at))->diffForHumans()}} 
                                    <?php foreach ($article->blogCategories as $category) { ?>
                                        <span>{{$category->title}}</span>
                                    <?php } ?>
                                </p>
                                <h3>{{$article->title}}</h3>
                            </div>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div> 
    <div class="col-sm-12 more-articles">
        <div class="container" id='all-articles'>
            <h1 class="heading">More Articles</h1>
            <?php foreach ($fiveArticles as $article) { ?>
                <div class="col-sm-12">
                    <div class="blog-article">
                        <h2 class="blog-title">{{$article->title}}</h2>
                        <div class="blog-publishment">
                            By <span class="blog-author">{{$article->articleAuthor->name}}</span>
                            <span class="blog-time">
                                {{date('F j, Y',strtotime($article->publish_at))}}
                                <?php foreach ($article->blogCategories as $category) { ?>
                                    <span>{{$category->title}}</span>
                                <?php } ?>
                            </span>
                        </div>
                        <div class="blog-img">
                            {{ HTML::image('img/backend/blogs/articles/'.$article->image) }}
                        </div>
                        <div class="blog-content">
                            <p><?php echo $article->excerpt; ?></p> 
                            <a href="{{ route('frontend.single-blog',$article->slug) }}" class="blog-read-more">Read More...</a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div> 
<!-- Modal -->
<div id="subscribe-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content--> 
        <div class="modal-content">
            <span class="close" data-dismiss="modal">&times;</span>
            <div class="modal-body">
                <p>Get the latest news and insights, delivered right to your inbox</p>
                {{ Form::open(array('url' => route('frontend.subscribe'),'method'=>'POST','id'=>'subscriptionForm')) }}
                {{ Form::email('email_address', '', ['class' => 'input-field', 'placeholder'=>'Your email']) }}
                {{ Form::submit('Submit', array('class' => 'btn-green')) }}
                {{ Form::close() }}
                <div class="server-response">
                </div> 
            </div>
        </div>
    </div> 
</div>
@endsection
@section('after-scripts')
<script id="single-article-html" type="text/html">
    <%  _.each(articles, function($article,key){%>
    <div class="col-sm-12 blog-article">
        <h2 class="blog-title"><%=$article.title%></h2>
        <div class="blog-publishment">
            By <span class="blog-author"><%=$article.article_author.name%></span>
            <span class="blog-time">
                <%= moment($article.publish_at, 'YYYY-MM-DD HH:mm:ss').format('MMMM D, YYYY')%>
                <%  _.each($article.blog_categories, function($category,key){%>
                <span><%=$category.title%></span>
                <% }) %>
            </span>
        </div>
        <div class="blog-img">
            {{ HTML::image('img/backend/blogs/articles/<%= $article.image %>') }}
        </div>
        <div class="blog-content">
            <p><%= $article.excerpt %></p>
            <a href="{{ route('frontend.single-blog') }}/<%=$article.slug %>" class="blog-read-more">Read More...</a>
        </div>
    </div>
    <% }) %>
</script>
<script>
    var getMoreBlogsRoute = "<?php echo route('frontend.getMoreBlogs'); ?>";
</script>
{{ Html::script(elixir('js/blog.js')) }}
@stop