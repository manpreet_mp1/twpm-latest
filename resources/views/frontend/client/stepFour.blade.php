<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(elixir('css/client.css')) }}   
@stop

@section('content')
<?php // dd('asf');  ?>

<div class="client">
    <div id="header">
        <div class="container-fluid account-setup-header">
            <div class="container">
                <div class="header-logo">
                    {{ HTML::image('img/Logo-wht.png') }}
                </div>
                <p>Account Setup</p>
            </div>
        </div>
    </div>

    <!--step 2 started-->
    <div id="body-content" class="container-fluid">
        <div class="container">
            <p class='content-head'>Step 3: About You</p>
            <div class="col-sm-8 left-bottom-border"></div> 
            <div class="col-sm-4 right-bottom-border"></div>
            <div class="inner-content">
                <h2 class='content-heading'>About You</h2>
                <p class='heading-info'>Don't worry, we'll never sell your info to anyone.</p>

                {{ Form::open(['route' =>'frontend.client.stepTwoRegister','class' => 'form-horizontal abc', 'role' => 'form', 'method' => 'post']) }}
                <!--{{ Form::text('first_name') }}-->
                <!--{{ Form::text('first_name', 'Chuck', array('class' => 'field')) }}-->

                <div class="form-group" id='about-you'>
                    <div class='social-security'>
                        {{ Form::number('ssn', null, ['class' => 'form-control social-security-number','required', 'placeholder' => 'Social security number']) }}
                        <p class="err-messages msg-err"><?php echo $errors->first('ssn'); ?></p>
                    </div>
                    <div class='social-security'>
                        {{ Form::number('dob', null, ['class' => 'form-control social-security-number','required', 'placeholder' => 'Date of birth (DD/MM/YYYY)']) }}
                        <p class="err-messages msg-err"><?php echo $errors->first('dob'); ?></p>
                    </div>
                    <div class='about-radio'>
                        <p>Gender</p>
                        {{ Form::radio('sex', 'male')}}Male<br>
                        {{ Form::radio('sex', 'female')}}Female
                    </div>
                    <div class="submit-btn" id='button-color'>
                        <button class="btn contact-info-button" type="submit">Continue</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('after-scripts')
@stop