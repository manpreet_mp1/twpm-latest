@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(elixir('css/client.css')) }}   
@stop

@section('content')
<div class="dashboard">

    <div id="dashboard-content">
        <div class="container-fluid">

            @include('frontend.includes.client_sidebar')

            <div class="right-content">
                @include('frontend.includes.client_header')

                <div class="col-sm-12 products-list">
                    <div class="col-sm-3 service-outer">
                        <div class="service-name">
                            <p>Product <br>Name</p>
                        </div>
                    </div>
                    <div class="col-sm-3 service-outer">
                        <div class="service-name">
                            <p>Product <br>Name</p>
                        </div>
                    </div>
                    <div class="col-sm-3 service-outer">
                        <div class="service-name">
                            <p>Product <br>Name</p>
                        </div>
                    </div>
                    <div class="col-sm-3 service-outer">
                        <div class="service-name">
                            <p>Product <br>Name</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-sm-6 dashboard-inner2">
                    <div id="my-tasks-list">
                        <h2>My Tasks</h2>
                        <ul class="list-unstyled">
                            <li>
                                You have a new task <span>Today</span>
                            </li>
                            <li>
                                You have a new task <span>Yesterday</span>
                            </li>
                            <li>
                                You have a new task <span class="red">April 13</span>
                            </li>
                        </ul>
                    </div>

                    <div id="document-activity">
                        <h2>Recent Document Activity</h2>
                        <div class="document">
                            <div class="image">
                                <i class="fa fa-file-pdf-o"></i>
                            </div>
                            <div class="details">
                                <p class="title">10435 fowef.pdf</p>
                                <ul class="list-inline">
                                    <li>Personal folder</li>
                                    <li>Earnging reports</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6 dashboard-inner2-right">
                    <div id="commentry">
                        <h2>Recent Commentary</h2>
                        <div class="comment">
                            <p class="date">April 12</p>
                            <p class="title">Risking Title</p>
                            <p class="desc">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            </p>
                            <a href="" class="arrow">
                                <i class="fa fa-chevron-right"></i>
                            </a>
                        </div>
                        <div class="clearfix"></div>

                        <div class="comment">
                            <p class="date">April 12</p>
                            <p class="title">Risking Title</p>
                            <p class="desc">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            </p>
                            <a href="" class="arrow">
                                <i class="fa fa-chevron-right"></i>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>    
@endsection

@section('after-scripts')
{{ Html::script(elixir('js/dashboard.js')) }}
@stop