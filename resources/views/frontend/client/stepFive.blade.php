@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(elixir('css/client.css')) }}   
@stop

@section('content')
<?php // die(route('legal')); ?>

<div class="client">
    <div id="header">
        <div class="container-fluid account-setup-header">
            <div class="container">
                <div class="header-logo">
                    {{ HTML::image('img/Logo-wht.png') }}
                </div>
                <p>Account Setup</p>
            </div>
        </div>
    </div>


    <!--step 2 started-->
    <div id="body-content" class="container-fluid">
        <div class="container">
            <p class='content-head'>Step 3: About You</p>
            <div class="col-sm-8 left-bottom-border"></div> 
            <div class="col-sm-4 right-bottom-border"></div>
            <div class="inner-content">

                {{ Form::open(['route' =>'frontend.client.stepTwoRegister','class' => 'form-horizontal abc', 'role' => 'form', 'method' => 'post']) }}
                <!--{{ Form::text('first_name') }}-->
                <!--{{ Form::text('first_name', 'Chuck', array('class' => 'field')) }}-->

                <div class="form-group" id='about-you-questionnaire'>
                    <div class='content-p-b'>
                        <h2>Divergent questionnaire dependent upon product</h2>
                    </div>
                    <div class="submit-btn" id='button-color'>
                        <button class="btn contact-info-button" type="submit">Finish up</button>
                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
@stop