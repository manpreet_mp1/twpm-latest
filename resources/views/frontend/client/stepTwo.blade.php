@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(elixir('css/client.css')) }}   
@stop

@section('content')
<?php // die(route('legal')); ?>
<div class="client">
<div id="header">
    <div class="container-fluid account-setup-header">
        <div class="container">
            <div class="header-logo">
                {{ HTML::image('img/Logo-wht.png') }}
            </div>
            <p>Account Setup</p>
        </div>
    </div>
</div>
<!--step 2 started-->

<div id="body-content" class="container-fluid">
    <div class="container">
        <p class='content-head'>Step 1: Contact Information</p>
        <div class="col-sm-3 left-bottom-border"></div> 
        <div class="col-sm-9 right-bottom-border"></div>
        <div class="inner-content">
            <h2 class='content-heading'>Contact Information</h2>
            <p class='heading-info'>Don't worry, we'll never sell your info to anyone.</p>

            {{ Form::open(['route' =>'frontend.client.stepTwoRegister','class' => 'form-horizontal abc', 'role' => 'form', 'method' => 'post']) }}
            <!--{{ Form::text('first_name') }}-->
            <!--{{ Form::text('first_name', 'Chuck', array('class' => 'field')) }}-->
             
             <input type="hidden" name="user_id" value ="@if( empty(old('user_id'))){{$userId}}@else{{old('user_id')}}@endif">
            <div class="form-group" id='content-information'>
                <div class='first-err'>
                    {{ Form::text('first_name', null, ['class' => 'form-control first-name','required', 'placeholder' => 'First name']) }}
                    <p class="err-messages msg-err"><?php echo $errors->first('first_name'); ?></p>
                </div>

                <div class="mi-err">
                    {{ Form::text('middle_name', null, ['class' => 'form-control m-i', 'placeholder' => 'MI']) }}
                    <p class="err-messages msg-err"><?php echo $errors->first('middle_name'); ?></p>
                </div>
                <div class='last-err'>
                    {{ Form::text('last_name', null, ['class' => 'form-control last-name','required', 'placeholder' => 'Last name']) }}
                    <p class="err-messages msg-err"><?php echo $errors->first('last_name'); ?></p>
                </div>

                <div class='first-address-err'>
                    {{ Form::text('first_address', null, ['class' => 'form-control first-address','required', 'placeholder' => 'First address']) }}
                    <p class="err-messages msg-err"><?php echo $errors->first('first_address'); ?></p>
                </div>

                <div class='last-address-err'>
                    {{ Form::text('last_address', null, ['class' => 'form-control last-address', 'placeholder' => 'Last address']) }}
                    <p class="err-messages msg-err"><?php echo $errors->first('last_address'); ?></p>
                </div> 

                <div class='city-err'>
                    {{ Form::text('city', null, ['class' => 'form-control city','required', 'placeholder' => 'City']) }}
                    <p class="err-messages msg-err"><?php echo $errors->first('city'); ?></p>
                </div>

                <div class='st-err'>
                    {{ Form::text('state', null, ['class' => 'form-control s-t','required', 'placeholder' => 'ST']) }}
                    <p class="err-messages msg-err"><?php echo $errors->first('state'); ?></p>
                </div>
                <div class='zip-code-err'>
                    {{ Form::number('zip', null, ['class' => 'form-control zip-code','required', 'placeholder' => 'Zip Code']) }}
                    <p class="err-messages msg-err"><?php echo $errors->first('zip'); ?></p>
                </div>

                <div class='phone-err'>
                    {{ Form::number('phone_number', null, ['class' => 'form-control phone','required', 'placeholder' => 'Phone']) }}
                    <p class="err-messages msg-err"><?php echo $errors->first('phone_number'); ?></p>
                </div>
                <div class="mobile-btn-err">
                    {{ Form::select('phone_type', config('constant.phone_type_inverse'), null, ['class' => 'btn btn-primary btn-sm mobile-button','required']) }}
                    <p class="err-messages msg-err"><?php echo $errors->first('phone_type'); ?></p>
                </div>
                <div class="submit-btn" id='button-color'>
                    <button class="btn contact-info-button" type="submit">Continue</button>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>
</div>
@endsection

@section('after-scripts')
@stop