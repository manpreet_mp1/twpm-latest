@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(elixir('css/client.css')) }}   
@stop

@section('content')

<div class="client">
    <div id="header">
        <div class="container-fluid account-setup-header">
            <div class="container">
                <div class="header-logo">
                    {{ HTML::image('img/Logo-wht.png') }}
                </div>
                <p>Account Setup</p>
            </div>
        </div>
    </div>
    <div id="body-content" class="container-fluid">
        <div class="container">
            <p class='content-head'>Step 1: Basic Information</p>
            <div class="col-sm-3 left-bottom-border"></div> 
            <div class="col-sm-9 right-bottom-border"></div>
            <div class="inner-content">
                <h2 class='content-heading'>Basic Information</h2>
                <p class='heading-info'>We use this information to create your account</p>

                {{ Form::open(['route' =>'frontend.client.stepOneRegister','class' => 'form-horizontal abc', 'role' => 'form', 'method' => 'post']) }}

                <div class="form-group" id='basic-information'>
                    <div class='basic-information-content'>
                        {{ Form::text('email', null, ['class' => 'form-control email_address common','required', 'placeholder' => 'Email Address']) }}
                        <p class="err-messages msg-err"><?php echo $errors->first('email'); ?></p>
                    </div>
                    <div class="password-head">
                        {{ Form::password('password', ['class' => 'form-control password common', 'placeholder' => 'Password(longer password are batter)']) }}
                        <p class="err-messages msg-err"><?php echo $errors->first('password'); ?></p>
                    </div>
                    <div class="submit-btn" id='button-color'>
                        <button class="btn contact-info-button" type="submit">Continue</button> 
                    </div>
                    <div class="terms-services">
                        <i>By joining, you agree to the<a href="#"> terms of service.</i></a>
                    </div>   
                </div>

                <a href="{{ route('frontend.client.signupStepOne', 'client') }}">Test1</a>

            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
@stop