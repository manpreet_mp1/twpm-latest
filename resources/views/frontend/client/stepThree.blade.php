<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(elixir('css/client.css')) }}   
@stop

@section('content')
<?php // dd('asf');  ?>
<div class="client">
    <div id="header">
        <div class="container-fluid account-setup-header">
            <div class="container">
                <div class="header-logo">
                    {{ HTML::image('img/Logo-wht.png') }}
                </div>
                <p>Account Setup</p>
            </div>
        </div>
    </div>

    <!--step 2 started-->
    <div id="body-content" class="container-fluid">
        <div class="container">
            <p class='content-head'>Step 2: Verify Mobile</p>
            <div class="col-sm-5 left-bottom-border"></div> 
            <div class="col-sm-7 right-bottom-border"></div>
            <div class="inner-content">
                <h2 class='content-heading'>Verify Mobile Number</h2>
                <p class='heading-info'>For added security and timely notifications.</p>

                {{ Form::open(['route' =>['frontend.client.signupStepFour','client'],'class' => 'form-horizontal abc', 'role' => 'form', 'method' => 'get']) }}
                <!--{{ Form::text('first_name') }}-->
                <!--{{ Form::text('first_name', 'Chuck', array('class' => 'field')) }}-->
                <div id='Verify-Mobile-Number'>
                    <div class="form-group"> 
                        <div class='mobile-number'>
                            {{ Form::number('phone_number', null, ['class' => 'form-control mobile-textbox','required', 'placeholder' => 'Enter your 10 digit mobile number']) }}
                            <p class="err-messages msg-err"><?php echo $errors->first('phone_number'); ?></p>
                        </div>
                        <div class="send-code">
                            <button class="btn btn-success send-code-button" type="submit">Send Code</button>
                        </div>
                        <div class="verify-inner-content">
                            <p>You'll receive a text message containing a code in a moment please enter that code below</p>
                        </div>

                        <div class='mobile-number'>
                            {{ Form::number('phone_number', null, ['class' => 'form-control mobile-textbox','required', 'placeholder' => '6 digit code']) }}
                            <p class="err-messages msg-err"><?php echo $errors->first('phone_number'); ?></p>
                        </div>
                        <div class="send-code">
                            <button class="btn btn-success send-code-button" type="submit">Verify</button>
                        </div>
                    </div>
                </div>
                <div class="submit-btn" id='button-color'>
                    <button class="btn contact-info-button" type="submit">Continue</button>
                </div>

                <div class='skip'>
                    <a href='#' class="skip-content">Skip this for now</a> 
                </div>


            </div>
        </div>
    </div>
</div>

@endsection

@section('after-scripts')
@stop