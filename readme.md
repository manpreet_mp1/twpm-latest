## Project details

Livesite: https://totalwpm.com/
Totalwpm is a comprehensive financial planning portal. I started working on the first version in 2016 and now it is on third version. We just deployed the third version.

## Technologies
- Laravel 5
- HTML5, CSS3, Jquery
- Stripe

## Setup Instructions
- Clone git
- Composer Install
- Copy the .env file "cp .env.example .env"
- Create database 
- Generate the Key "php artisan key:generate"
- NPM Install "npm install"
- Running Webpack "npm run dev"
- Seed the Database "php artisan migrate --seed"

## Laravel 5.* Boilerplate, Currently 5.3

[![Latest Stable Version](https://poser.pugx.org/rappasoft/laravel-5-boilerplate/v/stable)](https://packagist.org/packages/rappasoft/laravel-5-boilerplate) [![Latest Unstable Version](https://poser.pugx.org/rappasoft/laravel-5-boilerplate/v/unstable)](https://packagist.org/packages/rappasoft/laravel-5-boilerplate)

## System Requirements
- PHP > 7.1
- PHP Extensions: PDO, cURL, Mbstring, Tokenizer, Mcrypt, XML, GD
- Node.js > 6.0
- Composer > 1.0.0

