<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHomepageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('homepage', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('seo_title');
			$table->text('seo_description', 65535);
			$table->string('seo_img');
			$table->string('bg_image_1')->nullable();
			$table->text('heading_1', 65535);
			$table->string('subheading_1');
			$table->text('heading_3', 65535);
			$table->string('subheading_3');
			$table->string('image_4')->nullable();
			$table->text('heading_4', 65535);
			$table->string('description_4');
			$table->string('linktext_4');
			$table->string('bg_image_6')->nullable();
			$table->string('heading_6');
			$table->string('description_6');
			$table->string('linktext_6');
			$table->string('heading_7');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('homepage');
	}

}
