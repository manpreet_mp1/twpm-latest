<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToResourceTopicsFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('resource_topics_files', function(Blueprint $table)
		{
			$table->foreign('resource_topic_id', 'resource_topics_files_ibfk_1')->references('id')->on('resource_topics')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('resource_topics_files', function(Blueprint $table)
		{
			$table->dropForeign('resource_topics_files_ibfk_1');
		});
	}

}
