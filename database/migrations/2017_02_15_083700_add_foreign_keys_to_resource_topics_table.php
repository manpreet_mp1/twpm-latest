<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToResourceTopicsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('resource_topics', function(Blueprint $table)
		{
			$table->foreign('resource_category_id', 'resource_topics_ibfk_1')->references('id')->on('resource_categories')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('resource_topics', function(Blueprint $table)
		{
			$table->dropForeign('resource_topics_ibfk_1');
		});
	}

}
