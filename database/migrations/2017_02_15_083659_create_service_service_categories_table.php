<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServiceServiceCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_service_categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('service_id')->unsigned()->index('service_id');
			$table->integer('service_category_id')->unsigned()->index('service_category_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('service_service_categories');
	}

}
