<?php

return [
    'backend' => [
        'blogs' => [
            'default_status' => 0,
            'active' => 1
        ]
    ],
    'phone_type' => [
        'Mobile' => 1,
        'Home' => 2,
        'Work' => 3,
        'Other' => 4
    ],
    'phone_type_inverse' => [
        1 => 'Mobile',
        2 => 'Home',
        3 => 'Work',
        4 => 'Other'
    ],
];
